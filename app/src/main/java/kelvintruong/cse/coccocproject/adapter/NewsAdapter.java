package kelvintruong.cse.coccocproject.adapter;

import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import kelvintruong.cse.coccocproject.R;
import kelvintruong.cse.coccocproject.callback.NewsClickListener;
import kelvintruong.cse.coccocproject.rss.model.NewsModel;
import kelvintruong.cse.coccocproject.util.XMLUtil;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private ArrayList<NewsModel> mNewsList = new ArrayList<>();
    private NewsClickListener mClickListener;
    private boolean isDarkMode = false;

    public NewsAdapter(NewsClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.news_item_list, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        NewsModel newsModel = mNewsList.get(position);
        holder.binData(newsModel);
    }

    @Override
    public int getItemCount() {
        return mNewsList.size();
    }

    public void addNewsList(ArrayList<NewsModel> mNewsList) {
        this.mNewsList.addAll(mNewsList);
        notifyDataSetChanged();
    }

    public void setDarkMode(boolean darkMode) {
        isDarkMode = darkMode;
        notifyDataSetChanged();
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {

        private TextView txtTitle;
        private TextView txtDescription;
        private TextView txtPubDate;
        private ImageView imgDescription;

        NewsViewHolder(@NonNull final View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    NewsModel newsModel = (NewsModel) itemView.getTag();
                    if (mClickListener != null) {
                        mClickListener.onNewsSelected(newsModel);
                    }
                }
            });

            txtTitle = itemView.findViewById(R.id.title);
            txtPubDate = itemView.findViewById(R.id.pubDate);
            imgDescription = itemView.findViewById(R.id.imgDescription);
            txtDescription = itemView.findViewById(R.id.txtDescription);

        }

        void binData(NewsModel model) {

            itemView.setTag(model);

            txtTitle.setText(model.title);
            txtPubDate.setText(model.pubDate);
            txtDescription.setText(Html.fromHtml(model.description));
            Glide.with(itemView).load(XMLUtil.getImageUrl(model.description))
                    .placeholder(R.mipmap.ic_launcher_round)
                    .error(R.mipmap.ic_launcher_round)
                    .into(imgDescription);

            if (isDarkMode) {
                txtTitle.setTextColor(Color.WHITE);
                txtPubDate.setTextColor(Color.WHITE);
                txtDescription.setTextColor(Color.WHITE);
                itemView.setBackgroundColor(Color.BLACK);
            } else {
                txtTitle.setTextColor(Color.BLACK);
                txtPubDate.setTextColor(Color.BLACK);
                txtDescription.setTextColor(Color.BLACK);
                itemView.setBackgroundColor(Color.WHITE);
            }
        }
    }
}
