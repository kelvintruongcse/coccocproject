package kelvintruong.cse.coccocproject;

import android.os.Bundle;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {

    public static final String URL_KEY = "URL_KEY";

    private String mUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        WebView mWebView = findViewById(R.id.webView);
        if (getIntent().getExtras() != null) {
            mUrl = getIntent().getExtras().getString(URL_KEY);
        }
        mWebView.loadUrl(mUrl);
    }
}
