package kelvintruong.cse.coccocproject.callback;

import kelvintruong.cse.coccocproject.rss.model.NewsModel;

public interface NewsClickListener {
    public void onNewsSelected(NewsModel model);
}
