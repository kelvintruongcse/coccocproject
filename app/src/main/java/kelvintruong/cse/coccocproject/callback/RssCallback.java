package kelvintruong.cse.coccocproject.callback;

import java.util.ArrayList;

import kelvintruong.cse.coccocproject.rss.model.NewsModel;

public interface RssCallback {
    void onSuccess(ArrayList<NewsModel> newsList);
    void onFail(Exception e);
}
