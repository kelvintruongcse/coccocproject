package kelvintruong.cse.coccocproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;

import java.util.ArrayList;

import kelvintruong.cse.coccocproject.adapter.NewsAdapter;
import kelvintruong.cse.coccocproject.callback.NewsClickListener;
import kelvintruong.cse.coccocproject.callback.RssCallback;
import kelvintruong.cse.coccocproject.rss.model.NewsModel;
import kelvintruong.cse.coccocproject.rss.reader.RssReader;
import kelvintruong.cse.coccocproject.util.ShareUtil;

public class MainActivity extends AppCompatActivity implements RssCallback,
        NewsClickListener {

    Button mBtnRetry;
    RecyclerView mRvNews;
    NewsAdapter newsAdapter;
    SwitchCompat switchDarkMode;
    boolean isEnableDarkMode = false;

    private View.OnClickListener onRetryClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            loadRss();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isEnableDarkMode = ShareUtil.isDarkMode(this);

        mBtnRetry = findViewById(R.id.btnRetry);
        mBtnRetry.setOnClickListener(onRetryClickListener);

        mRvNews = findViewById(R.id.rvNews);
        switchDarkMode = findViewById(R.id.swtNightMode);
        switchDarkMode.setChecked(isEnableDarkMode);
        switchDarkMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                newsAdapter.setDarkMode(checked);

                ShareUtil.setDarkMode(MainActivity.this, checked);
                if (checked) {
                    setTheme(R.style.ActivityTheme_Primary_Base_Dark);
                } else {
                    setTheme(R.style.ActivityTheme_Primary_Base_Light);
                }

                recreate();
            }
        });

        initRvNews();
    }


    private void loadRss() {
        String url = getString(R.string.rss_host);
        RssReader rssReader = new RssReader(url, this);
        rssReader.execute();
    }

    private void initRvNews() {
        newsAdapter = new NewsAdapter(this);
        newsAdapter.setDarkMode(isEnableDarkMode);
        mRvNews.setAdapter(newsAdapter);
        mRvNews.setLayoutManager(new LinearLayoutManager(this));
        loadRss();
    }


    @Override
    public void onSuccess(ArrayList<NewsModel> newsList) {
        if (!newsList.isEmpty()) {
            mBtnRetry.setVisibility(View.GONE);
            mRvNews.setVisibility(View.VISIBLE);
            newsAdapter.addNewsList(newsList);
        } else {
            mBtnRetry.setVisibility(View.VISIBLE);
            mRvNews.setVisibility(View.GONE);
        }
    }

    @Override
    public void onFail(Exception e) {

    }

    @Override
    public void onNewsSelected(NewsModel model) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.URL_KEY, model.link);
        startActivity(intent);
    }
}
