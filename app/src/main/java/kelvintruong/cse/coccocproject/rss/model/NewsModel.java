package kelvintruong.cse.coccocproject.rss.model;

public class NewsModel {
    public String description;
    public String title;
    public String link;
    public String pubDate;

    public NewsModel(String description, String title, String link, String pubDate) {
        this.description = description;
        this.title = title;
        this.link = link;
        this.pubDate = pubDate;
    }
}
