package kelvintruong.cse.coccocproject.rss.reader;

import android.os.AsyncTask;

import org.w3c.dom.Document;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import kelvintruong.cse.coccocproject.callback.RssCallback;
import kelvintruong.cse.coccocproject.rss.model.NewsModel;
import kelvintruong.cse.coccocproject.util.XMLUtil;

public class RssReader extends AsyncTask<Void, Void, ArrayList<NewsModel>> {

    private String mUrl;
    private RssCallback mCallback;

    public RssReader(String url, RssCallback callback) {
        this.mUrl = url;
        this.mCallback = callback;
    }

    @Override
    protected ArrayList<NewsModel> doInBackground(Void... voids) {
        try {
            InputStream inputStream = new URL(mUrl).openConnection().getInputStream();
            Document document = XMLUtil.getDocumentFromStream(inputStream);
            return XMLUtil.getNewsList(document);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<NewsModel> newsList) {
        super.onPostExecute(newsList);
        if (mCallback != null) {
            mCallback.onSuccess(newsList);
        }
    }
}
