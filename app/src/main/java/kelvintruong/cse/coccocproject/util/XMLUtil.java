package kelvintruong.cse.coccocproject.util;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import kelvintruong.cse.coccocproject.rss.model.NewsModel;

public class XMLUtil {
    public static Document getDocumentFromStream(InputStream inputStream) {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            return builder.parse(new InputSource(inputStream));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<NewsModel> getNewsList(Document document) throws XPathExpressionException {
        ArrayList<NewsModel> articleList = new ArrayList<>();
        XPath xPath = XPathFactory.newInstance().newXPath();
        String expression = "//channel/item";

        NodeList itemNodeList = (NodeList) xPath.compile(expression).evaluate(document, XPathConstants.NODESET);

        for (int i = 0; i < itemNodeList.getLength(); i++) {
            Node nodeItem = itemNodeList.item(i);

            String title = "";
            String description = "";
            String imgUrl = "";
            String pubDate = "";
            String link = "";

            for (int j = 0; j < nodeItem.getChildNodes().getLength(); j++) {
                Node node = nodeItem.getChildNodes().item(j);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    if (node.hasChildNodes()) {

                        // Title
                        if (node.getNodeName().equals("title")) {
                            title = node.getFirstChild().getNodeValue();
                        }

                        // Description
                        if (node.getNodeName().equals("description")) {
                            Node firstNode = node.getFirstChild();
                            description = node.getFirstChild().getNodeValue();
                        }

                        // Publish date
                        if (node.getNodeName().equals("pubDate")) {
                            pubDate = node.getFirstChild().getNodeValue();
                        }

                        // Link
                        if (node.getNodeName().equals("link")) {
                            link = node.getFirstChild().getNodeValue();
                        }
                    }
                }
            }
            articleList.add(new NewsModel(description, title, link, pubDate));
        }
        return articleList;
    }

    public static String getImageUrl(String description) {

        String imgLink;
        org.jsoup.nodes.Document doc = Jsoup.parse(description);
        Elements element = doc.getElementsByTag("img");

        if (element == null
                || element.first() == null
                || element.first().attributes() == null) {
            return "";
        }

        imgLink = element.first().attributes().get("src");

        if (imgLink.startsWith("data:image")) {
            imgLink = element.first().attributes().get("data-original");
        }

        return imgLink;
    }
}
