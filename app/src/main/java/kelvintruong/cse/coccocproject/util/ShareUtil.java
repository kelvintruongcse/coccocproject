package kelvintruong.cse.coccocproject.util;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class ShareUtil {
    public static void setDarkMode(Context context, boolean enableDarkMode) {
        SharedPreferences.Editor editor = context.getSharedPreferences("cococproject", MODE_PRIVATE)
                .edit();
        editor.putBoolean("DarkMode", enableDarkMode);
        editor.apply();
    }

    public static boolean isDarkMode(Context context){
        SharedPreferences prefs = context.getSharedPreferences("cococproject", MODE_PRIVATE);
        return prefs.getBoolean("DarkMode", false);
    }
}
